<?php
/**
 * Zakra functions and definitions
 *
 * @link    https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package zakra
 */

if ( ! function_exists( 'zakra_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function zakra_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on zakra, use a find and replace
		 * to change 'zakra' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'zakra', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-primary' => esc_html__( 'Primary', 'zakra' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'width'       => 170,
			'height'      => 60,
			'flex-width'  => true,
			'flex-height' => true,
		) );

		/**
		 * Custom background support.
		 */
		add_theme_support( 'custom-background' );

		/**
		 * Gutenberg Wide/fullwidth support.
		 */
		add_theme_support( 'align-wide' );

		/**
		 * AMP support.
		 */
		if ( defined( 'AMP__VERSION' ) && ( ! version_compare( AMP__VERSION, '1.0.0', '<' ) ) ) {
			add_theme_support( 'amp',
				apply_filters(
					'zakra_amp_support_filter',
					array(
						'paired' => true,
					)
				)
			);
		}
	}
endif;
add_action( 'after_setup_theme', 'zakra_setup' );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function zakra_widgets_init() {
	$sidebars = apply_filters( 'zakra_sidebars_args', array(
		'header-top-left-sidebar'  => esc_html__( 'Header Top Bar Left Sidebar', 'zakra' ),
		'header-top-right-sidebar' => esc_html__( 'Header Top Bar Right Sidebar', 'zakra' ),
		'sidebar-right'            => esc_html__( 'Sidebar Right', 'zakra' ),
		'sidebar-left'             => esc_html__( 'Sidebar Left', 'zakra' ),
		'footer-sidebar-1'         => esc_html__( 'Footer One', 'zakra' ),
		'footer-sidebar-2'         => esc_html__( 'Footer Two', 'zakra' ),
		'footer-sidebar-3'         => esc_html__( 'Footer Three', 'zakra' ),
		'footer-sidebar-4'         => esc_html__( 'Footer Four', 'zakra' ),
		'footer-bar-left-sidebar'  => esc_html__( 'Footer Bottom Bar Left Sidebar', 'zakra' ),
		'footer-bar-right-sidebar' => esc_html__( 'Footer Bottom Bar Right Sidebar', 'zakra' ),
	) );

	if ( zakra_is_woocommerce_active() ) {
		$sidebars['wc-left-sidebar']  = esc_html__( 'WooCommerce Left Sidebar', 'zakra' );
		$sidebars['wc-right-sidebar'] = esc_html__( 'WooCommerce Right Sidebar', 'zakra' );
	}

	foreach ( $sidebars as $id => $name ) :

		register_sidebar( array(
			'id'            => $id,
			'name'          => $name,
			'description'   => esc_html__( 'Add widgets here.', 'zakra' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		) );

	endforeach;

}

add_action( 'widgets_init', 'zakra_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function zakra_scripts() {
	$suffix = ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ) ? '' : '.min';

	/**
	 * Styles.
	 */
	// Font Awesome 4.
	wp_register_style( 'font-awesome', get_template_directory_uri() . '/assets/lib/font-awesome/css/font-awesome' . $suffix . '.css', false, '4.7.0' );
	wp_enqueue_style( 'font-awesome' );

	// Theme style.
	wp_register_style( 'zakra-style', get_stylesheet_uri() );
	wp_enqueue_style( 'zakra-style' );

	// Support RTL.
	wp_style_add_data( 'zakra-style', 'rtl', 'replace' );

	// Do not load scripts if AMP.
	if ( zakra_is_amp() ) {
		return;
	}
	/**
	 * Scripts.
	 */
	wp_enqueue_script( 'zakra-navigation', get_template_directory_uri() . '/assets/js/navigation' . $suffix . '.js', array(), '20151215', true );
	wp_enqueue_script( 'zakra-skip-link-focus-fix', get_template_directory_uri() . '/assets/js/skip-link-focus-fix' . $suffix . '.js', array(), '20151215', true );
	// Theme JavaScript.
	wp_enqueue_script( 'zakra-custom', get_template_directory_uri() . '/assets/js/zakra-custom' . $suffix . '.js', array(), false, true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
	 $address = 'balt# 2 germany';
	wp_localize_script( 'address_obj', 'user_address', $address );
 
	// Enqueued script with localized data.
	wp_enqueue_script( 'address_obj' );
	
}

add_action( 'wp_enqueue_scripts', 'zakra_scripts' );

/**
 * Define constants
 */
// Root path/URI.
define( 'ZAKRA_PARENT_DIR', get_template_directory() );
define( 'ZAKRA_PARENT_URI', get_template_directory_uri() );

// Include path/URI.
define( 'ZAKRA_PARENT_INC_DIR', ZAKRA_PARENT_DIR . '/inc' );
define( 'ZAKRA_PARENT_INC_URI', ZAKRA_PARENT_URI . '/inc' );

// Icons path.
define( 'ZAKRA_PARENT_INC_ICON_URI', ZAKRA_PARENT_URI . '/assets/img/icons' );
// Customizer path.
define( 'ZAKRA_PARENT_CUSTOMIZER_DIR', ZAKRA_PARENT_INC_DIR . '/customizer' );

// Theme version.
$zakra_theme = wp_get_theme();
define( 'ZAKRA_THEME_VERSION', $zakra_theme->get( 'Version' ) );

/**
 * AMP support files.
 */
if ( defined( 'AMP__VERSION' ) && ( ! version_compare( AMP__VERSION, '1.0.0', '<' ) ) ) {
	require_once ZAKRA_PARENT_INC_DIR . '/compatibility/amp/amp.php';
}

/**
 * Helper functions.
 */
require ZAKRA_PARENT_INC_DIR . '/helpers.php';

/**
 * Implement the Custom Header feature.
 */
require ZAKRA_PARENT_INC_DIR . '/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require ZAKRA_PARENT_INC_DIR . '/class-zakra-dynamic-filter.php';
require ZAKRA_PARENT_INC_DIR . '/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require ZAKRA_PARENT_INC_DIR . '/template-functions.php';

/**
 * Customizer additions.
 */
require ZAKRA_PARENT_INC_DIR . '/customizer/class-zakra-customizer.php';
require ZAKRA_PARENT_INC_DIR . '/class-zakra-css-classes.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require ZAKRA_PARENT_INC_DIR . '/jetpack.php';
}

/**
 * WooCommerce hooks and functions.
 */
if ( class_exists( 'WooCommerce' ) ) {
	require ZAKRA_PARENT_INC_DIR . '/woocommerce.php';
}

/**
 * Load hooks.
 */
require ZAKRA_PARENT_INC_DIR . '/hooks/hooks.php';
require ZAKRA_PARENT_INC_DIR . '/hooks/header.php';
require ZAKRA_PARENT_INC_DIR . '/hooks/footer.php';
require ZAKRA_PARENT_INC_DIR . '/hooks/content.php';
require ZAKRA_PARENT_INC_DIR . '/migration.php';

/**
 * Breadcrumbs class.
 */
require_once ZAKRA_PARENT_INC_DIR . '/class-breadcrumb-trail.php';

/**
 * Metaboxes.
 */
require ZAKRA_PARENT_INC_DIR . '/meta-boxes/class-zakra-meta-box-page-settings.php';
require ZAKRA_PARENT_INC_DIR . '/meta-boxes/class-zakra-meta-box.php';

/**
 * Theme options page.
 */
if ( is_admin() ) {
	require ZAKRA_PARENT_INC_DIR . '/admin/about-page/class-zakra-plugin-install-helper.php';
	require ZAKRA_PARENT_INC_DIR . '/admin/about-page/class-zakra-about-page.php';
	require ZAKRA_PARENT_INC_DIR . '/admin/class-zakra-admin.php';
	require ZAKRA_PARENT_INC_DIR . '/admin/about-page/class-zakra-site-library.php';
	require ZAKRA_PARENT_INC_DIR . '/admin/class-zakra-theme-review-notice.php';
}

/**
 * Detect plugin. For use on Front End only.
 */
include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

/**
 * Set default content width.
 */
if ( ! isset( $content_width ) ) {

	$content_width = 812;

}

/**
 * Calculate $content_width value according to layout options from Customizer and meta boxes.
 */
function zakra_content_width_rdr() {

	global $content_width;

	// Get layout type.
	$layout_type     = zakra_get_layout_type();
	$layouts_sidebar = array( 'tg-site-layout--left', 'tg-site-layout--right' );

	/**
	 * Calculate content width.
	 */
	// Get required values from Customizer.
	$container_width_arr = get_theme_mod( 'zakra_general_container_width', array(
		'slider' => 1160,
		'suffix' => 'px',
	) );
	$content_width_arr   = get_theme_mod( 'zakra_general_content_width', array(
		'slider' => 70,
		'suffix' => '%',
	) );

	// Calculate Padding to reduce.
	$container_style = get_theme_mod( 'zakra_general_container_style', 'tg-container--wide' );
	$content_padding = ( 'tg-container--separate' === $container_style ) ? 120 : 60;

	if ( in_array( $layout_type, $layouts_sidebar, true ) ) {

		$content_width = ( ( (int) $container_width_arr['slider'] * (int) $content_width_arr['slider'] ) / 100 ) - $content_padding;

	} else {
		$content_width = (int) $container_width_arr['slider'] - $content_padding;
	}

}

add_action( 'template_redirect', 'zakra_content_width_rdr' );

if ( ! function_exists( 'zakra_stretched_style_migrate' ) ) :
	/**
	 * Migrate `Stretched` container style to `Layout`.
	 */
	function zakra_stretched_style_migrate() {

		$container_style = get_theme_mod( 'zakra_general_container_style', 'tg-container--wide' );

		$layout_arr = array( 'tg-site-layout--left', 'tg-site-layout--right' );

		$page_types = array( 'default', 'archive', 'post', 'page' );

		// Lets bail out if container style is not stretched.
		if ( 'tg-container--stretched' != $container_style ) {
			return;
		}

		// Lets bail out if 'zakra_stretched_style_transfer' option found.
		if ( get_option( 'zakra_stretched_style_transfer' ) ) {
			return;
		}

		set_theme_mod( 'zakra_general_container_style', 'tg-container--wide' );

		foreach ( $page_types as $page_type ) {
			$layout = get_theme_mod( 'zakra_structure_' . $page_type, 'tg-site-layout--right' );

			// Do nothing if left or right sidebar enabled.
			if ( ! in_array( $layout, $layout_arr ) ) {
				set_theme_mod( 'zakra_structure_' . $page_type, 'tg-site-layout--stretched' );
			}
		}

		// Set transfer as complete.
		update_option( 'zakra_stretched_style_transfer', 1 );

	}
endif;
add_action( 'after_setup_theme', 'zakra_stretched_style_migrate' );

add_action('init', 'wp_address_form');
function wp_address_form() {

		

   add_shortcode('show_address_form', 'wp_show_address_form');
}
function wp_show_address_form($atts) {
	//$address = 'asdasdasd asdasdas';
	
ob_start();
$steps_url = site_url().'/steps';
?>
<div class="et_pb_text_inner">
<p>
<span class="header-form-field">
<input id="middle-address" class="location-search" type="text" placeholder="Enter your Home Address" autocomplete="off"></span>
<span class="header-form-btn">
<input class="offer-btn" id="submit_address" type="button" value="Get Your Real Offer">
</span>
</p>
</div>
<script>
<?php ?>

jQuery( "#submit_address" ).click(function() {
  	var adress = jQuery("#middle-address").val();
    localStorage.setItem('user_address', adress);
	window.location.href = '<?php echo $steps_url; ?>';
});
</script>
<?php

return ob_get_clean();

}
function hook_javascript() {
	if(is_page('steps')){
		?>
			<script>
			jQuery( document ).ready(function() {
				var user_address = localStorage.getItem('user_address');
				console.log(user_address);
				//jQuery("#msf-text-address").attr('value','');
				jQuery("#msf-text-address").val(user_address);
			});
			</script>
		<?php
	}
}
add_action('wp_head', 'hook_javascript');


require_once get_template_directory() . '/inc/class-tgm-plugin-activation.php';

add_action( 'tgmpa_register', 'kevin_theme_register_required_plugins' );

/**
 * Register the required plugins for this theme.
 *
 * In this example, we register five plugins:
 * - one included with the TGMPA library
 * - two from an external source, one from an arbitrary source, one from a GitHub repository
 * - two from the .org repo, where one demonstrates the use of the `is_callable` argument
 *
 * The variables passed to the `tgmpa()` function should be:
 * - an array of plugin arrays;
 * - optionally a configuration array.
 * If you are not changing anything in the configuration array, you can remove the array and remove the
 * variable from the function call: `tgmpa( $plugins );`.
 * In that case, the TGMPA default settings will be used.
 *
 * This function is hooked into `tgmpa_register`, which is fired on the WP `init` action on priority 10.
 */
function kevin_theme_register_required_plugins() {
	/*
	 * Array of plugin arrays. Required keys are name and slug.
	 * If the source is NOT from the .org repo, then source is also required.
	 */
	$plugins = array(

		// This is an example of how to include a plugin bundled with a theme.
		array(
			'name'               => 'Kevin Demo Importer', // The plugin name.
			'slug'               => 'tgm-example-plugin', // The plugin slug (typically the folder name).
			'source'             => get_template_directory() . '/import/plugins/tgm-example-plugin.zip', // The plugin source.
			'required'           => true, // If false, the plugin is only 'recommended' instead of required.
			'version'            => '', // E.g. 1.0.0. If set, the active plugin must be this version or higher. If the plugin version is higher than the plugin version installed, the user will be notified to update the plugin.
			'force_activation'   => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch.
			'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins.
			'external_url'       => '', // If set, overrides default API URL and points to an external URL.
			'is_callable'        => '', // If set, this callable will be be checked for availability to determine if a plugin is active.
		),

		// This is an example of how to include a plugin from an arbitrary external source in your theme.
		array(
			'name'         => 'Contact Form 7', // The plugin name.
			'slug'         => 'contact-form-7', // The plugin slug (typically the folder name).
			'source'       => '', // The plugin source.
			'required'     => true, // If false, the plugin is only 'recommended' instead of required.
			'external_url' => '', // If set, overrides default API URL and points to an external URL.
		),
		// This is an example of how to include a plugin from an arbitrary external source in your theme.
		array(
			'name'         => 'Elementor Page Builder', // The plugin name.
			'slug'         => 'elementor', // The plugin slug (typically the folder name).
			'source'       => '', // The plugin source.
			'required'     => true, // If false, the plugin is only 'recommended' instead of required.
			'external_url' => '', // If set, overrides default API URL and points to an external URL.
		),
		// This is an example of how to include a plugin from an arbitrary external source in your theme.
		array(
			'name'         => 'Multi Step Form', // The plugin name.
			'slug'         => 'multi-step-form', // The plugin slug (typically the folder name).
			'source'       => '', // The plugin source.
			'required'     => true, // If false, the plugin is only 'recommended' instead of required.
			'external_url' => '', // If set, overrides default API URL and points to an external URL.
		),
		// This is an example of how to include a plugin from an arbitrary external source in your theme.
		array(
			'name'         => 'One Click Demo Import', // The plugin name.
			'slug'         => 'one-click-demo-import', // The plugin slug (typically the folder name).
			'source'       => '', // The plugin source.
			'required'     => true, // If false, the plugin is only 'recommended' instead of required.
			'external_url' => '', // If set, overrides default API URL and points to an external URL.
		),

		// This is an example of how to include a plugin from a GitHub repository in your theme.
		// This presumes that the plugin code is based in the root of the GitHub repository
		// and not in a subdirectory ('/src') of the repository.
		// array(
		// 	'name'      => 'Adminbar Link Comments to Pending',
		// 	'slug'      => 'adminbar-link-comments-to-pending',
		// 	'source'    => 'https://github.com/jrfnl/WP-adminbar-comments-to-pending/archive/master.zip',
		// ),

		// This is an example of how to include a plugin from the WordPress Plugin Repository.
		// array(
		// 	'name'      => 'BuddyPress',
		// 	'slug'      => 'buddypress',
		// 	'required'  => false,
		// ),

		// This is an example of the use of 'is_callable' functionality. A user could - for instance -
		// have WPSEO installed *or* WPSEO Premium. The slug would in that last case be different, i.e.
		// 'wordpress-seo-premium'.
		// By setting 'is_callable' to either a function from that plugin or a class method
		// `array( 'class', 'method' )` similar to how you hook in to actions and filters, TGMPA can still
		// recognize the plugin as being installed.
		// array(
		// 	'name'        => 'WordPress SEO by Yoast',
		// 	'slug'        => 'wordpress-seo',
		// 	'is_callable' => 'wpseo_init',
		// ),

	);

	/*
	 * Array of configuration settings. Amend each line as needed.
	 *
	 * TGMPA will start providing localized text strings soon. If you already have translations of our standard
	 * strings available, please help us make TGMPA even better by giving us access to these translations or by
	 * sending in a pull-request with .po file(s) with the translations.
	 *
	 * Only uncomment the strings in the config array if you want to customize the strings.
	 */
	$config = array(
		'id'           => 'kevin-theme',           // Unique ID for hashing notices for multiple instances of TGMPA.
		'default_path' => '',                      // Default absolute path to bundled plugins.
		'menu'         => 'tgmpa-install-plugins', // Menu slug.
		'parent_slug'  => 'themes.php',            // Parent menu slug.
		'capability'   => 'edit_theme_options',    // Capability needed to view plugin install page, should be a capability associated with the parent menu used.
		'has_notices'  => true,                    // Show admin notices or not.
		'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
		'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
		'is_automatic' => false,                   // Automatically activate plugins after installation or not.
		'message'      => '',                      // Message to output right before the plugins table.

		/*
		'strings'      => array(
			'page_title'                      => __( 'Install Required Plugins', 'kevin-theme' ),
			'menu_title'                      => __( 'Install Plugins', 'kevin-theme' ),
			/* translators: %s: plugin name. * /
			'installing'                      => __( 'Installing Plugin: %s', 'kevin-theme' ),
			/* translators: %s: plugin name. * /
			'updating'                        => __( 'Updating Plugin: %s', 'kevin-theme' ),
			'oops'                            => __( 'Something went wrong with the plugin API.', 'kevin-theme' ),
			'notice_can_install_required'     => _n_noop(
				/* translators: 1: plugin name(s). * /
				'This theme requires the following plugin: %1$s.',
				'This theme requires the following plugins: %1$s.',
				'kevin-theme'
			),
			'notice_can_install_recommended'  => _n_noop(
				/* translators: 1: plugin name(s). * /
				'This theme recommends the following plugin: %1$s.',
				'This theme recommends the following plugins: %1$s.',
				'kevin-theme'
			),
			'notice_ask_to_update'            => _n_noop(
				/* translators: 1: plugin name(s). * /
				'The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.',
				'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.',
				'kevin-theme'
			),
			'notice_ask_to_update_maybe'      => _n_noop(
				/* translators: 1: plugin name(s). * /
				'There is an update available for: %1$s.',
				'There are updates available for the following plugins: %1$s.',
				'kevin-theme'
			),
			'notice_can_activate_required'    => _n_noop(
				/* translators: 1: plugin name(s). * /
				'The following required plugin is currently inactive: %1$s.',
				'The following required plugins are currently inactive: %1$s.',
				'kevin-theme'
			),
			'notice_can_activate_recommended' => _n_noop(
				/* translators: 1: plugin name(s). * /
				'The following recommended plugin is currently inactive: %1$s.',
				'The following recommended plugins are currently inactive: %1$s.',
				'kevin-theme'
			),
			'install_link'                    => _n_noop(
				'Begin installing plugin',
				'Begin installing plugins',
				'kevin-theme'
			),
			'update_link' 					  => _n_noop(
				'Begin updating plugin',
				'Begin updating plugins',
				'kevin-theme'
			),
			'activate_link'                   => _n_noop(
				'Begin activating plugin',
				'Begin activating plugins',
				'kevin-theme'
			),
			'return'                          => __( 'Return to Required Plugins Installer', 'kevin-theme' ),
			'plugin_activated'                => __( 'Plugin activated successfully.', 'kevin-theme' ),
			'activated_successfully'          => __( 'The following plugin was activated successfully:', 'kevin-theme' ),
			/* translators: 1: plugin name. * /
			'plugin_already_active'           => __( 'No action taken. Plugin %1$s was already active.', 'kevin-theme' ),
			/* translators: 1: plugin name. * /
			'plugin_needs_higher_version'     => __( 'Plugin not activated. A higher version of %s is needed for this theme. Please update the plugin.', 'kevin-theme' ),
			/* translators: 1: dashboard link. * /
			'complete'                        => __( 'All plugins installed and activated successfully. %1$s', 'kevin-theme' ),
			'dismiss'                         => __( 'Dismiss this notice', 'kevin-theme' ),
			'notice_cannot_install_activate'  => __( 'There are one or more required or recommended plugins to install, update or activate.', 'kevin-theme' ),
			'contact_admin'                   => __( 'Please contact the administrator of this site for help.', 'kevin-theme' ),

			'nag_type'                        => '', // Determines admin notice type - can only be one of the typical WP notice classes, such as 'updated', 'update-nag', 'notice-warning', 'notice-info' or 'error'. Some of which may not work as expected in older WP versions.
		),
		*/
	);

	tgmpa( $plugins, $config );
}


function ocdi_import_files() {
    return array(
        array(
            'import_file_name'             => 'Demo Import 1',
            'categories'                   => array( 'Category 1', 'Category 2' ),
            'local_import_file'            => trailingslashit( get_template_directory() ) . 'import/kevin-data.xml',
            'local_import_widget_file'     => '',
            'local_import_customizer_file' => trailingslashit( get_template_directory() ) . 'import/kevin-customizer.dat',
            'local_import_redux'           => array(
                array(
                    'file_path'   => trailingslashit( get_template_directory() ) . 'ocdi/redux.json',
                    'option_name' => 'redux_option_name',
                ),
            ),
            'import_preview_image_url'     => 'http://www.your_domain.com/ocdi/preview_import_image1.jpg',
            'import_notice'                => __( 'After you import this demo, you will have to setup the slider separately.', 'kevin-theme' ),
            'preview_url'                  => 'http://www.your_domain.com/my-demo-1',
        )
    );
}
add_filter( 'pt-ocdi/import_files', 'ocdi_import_files' );

function ocdi_after_import_setup() {
    // Assign menus to their locations.
    $main_menu = get_term_by( 'name', 'Main Menu', 'nav_menu' );

    $locations = get_theme_mod('nav_menu_locations');
	$locations['menu-primary'] = $main_menu->term_id;
	set_theme_mod( 'nav_menu_locations', $locations );

    // Assign front page and posts page (blog page).
    $front_page_id = get_page_by_title( 'Home' );
    $blog_page_id  = get_page_by_title( 'Blog' );

    update_option( 'show_on_front', 'page' );
    update_option( 'page_on_front', $front_page_id->ID );
    update_option( 'page_for_posts', $blog_page_id->ID );

}
add_action( 'pt-ocdi/after_import', 'ocdi_after_import_setup' );
